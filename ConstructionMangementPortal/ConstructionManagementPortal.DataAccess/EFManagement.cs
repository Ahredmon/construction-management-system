﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionManagementPortal.DataAccess
{

    public class EFManagement
    {
        ConstructManDBEntities1 x = new ConstructManDBEntities1();

        public IEnumerable<Person> GetEmployees()
        {
            return x.Person;
        }

        public IEnumerable<PersonTask> GetEmployeeTasks(int id)
        {
            return x.Person.First(e => e.ID == id).Task;
        }

        public IEnumerable<Product> GetProducts()
        {
            return x.Product;
        }

        public IEnumerable<ProjectTask> GetProjectTasks()
        {
            return x.Task1;
        }


        public IEnumerable<Vendor> GetVendors()
        {
            return x.Vendor;
        }

        public IEnumerable<Site> GetSites()
        {
            return x.Site;
        }

        public IEnumerable<AcceptedProject> GetProjects()
        {
            return x.AcceptedProject;
        }

        public IEnumerable<ProductCategory> GetProductCategories()
        {
            return x.Category1;
        }

        public IEnumerable<Person> GetManagement()
        {
            return x.Person.Where(e => e.ID >= 2);
        }

        public IEnumerable<Schedule> GetSchedule(int id)
        {
            return x.Schedule.Where(e => e.EmployeeID == id);
        }

        public Person GetEmployee(int ID)
        {
            return x.Person.First(e => e.ID == ID);
        }

        public IEnumerable<WorkDay> GetWorkDays()
        {
            return x.WorkDay;
        }
        public IEnumerable<WorkDay> GetWorkDays(int ID)
        {
            return x.WorkDay.Where(e=> e.EmployeeId == ID);
        }
        public bool ClockIn(int Id)
        {
            var clockedIn = false;
            var employee = x.Person.First(e => e.ID == Id);
            var day = new WorkDay()
            {
                Person = employee,
                Active = true,
                DateModified = DateTime.Now,
                TimeIn = DateTime.Now.TimeOfDay
            };
            var workdays = x.WorkDay.Where(e => e.EmployeeId == Id);
            foreach (var item in workdays)
            {
                
                if (item.DateModified.Value.Date== DateTime.Now.Date)
                {
                    clockedIn = true;
                    break;
                }

            }
            if (clockedIn)
            {
                return false;
            }
            else
            {
                x.WorkDay.Add(day);
                SaveChanges();
            }
            return true;
        }

        public bool ClockOut(int ID)
        {
            var clockedIn = false;
            var employee = GetEmployee(ID);
            var workdays = GetWorkDays(ID);
            foreach (var item in workdays)
            {
                if (item.DateModified.Value.Date == DateTime.Today)
                {
                    if (item.TimeOut != null)
                    {
                        return false;
                    }
                    item.TimeOut = DateTime.Now.TimeOfDay;
                    foreach (var task in x.Task.Where(e => e.EmployeeId == ID))
                    {
                        
                        if (task.StatusID == 6)
                        {
                            var temp = item.DateModified.Value.TimeOfDay;
                            var now = DateTime.Now.TimeOfDay;
                            var difference = now.Subtract(temp);
                            task.HoursWorked += difference.Duration().TotalHours;
                            var pt = x.Task1.First(m=> m.Id== task.ProjectId);
                            pt.HoursWorked += difference.Duration().TotalHours;
                            var project = x.AcceptedProject.First(m => m.Id == pt.ProjectID);
                            project.HoursWorked += difference.Duration().TotalHours;

                            task.StatusID = 8;
                        }
                    }
                   // Debug.WriteLine("clocking out");
                    clockedIn = true;
                    
                }
            }
            if (!clockedIn)
            {
                return false;
            }
            SaveChanges();
            return true;
        }

        public bool AddEmployee(Person p, Details d)
        {
            try
            {
               
                    Debug.WriteLine(p.RoleID);
                    Debug.WriteLine(d.username);
                d.SiteID = 1;
                p.Details = d;
                p.Role = GetRoles().First(e => e.ID == p.RoleID);
                    p.DetailsID = d.ID;
                    x.Person.AddOrUpdate(p);
              
                 
                   
                  
                
                
                
            }
            catch (Exception ex)
            {
                throw new UserNotUniqueException("Username must be Unique!", ex);
            }
            return SaveChanges();

        }

        public bool AddSite(Site s)
        {
            x.Site.Add(s);
            return SaveChanges();
        }

        public bool ChangeSiteAssignment(Person p, Site s)
        {
            var temp = x.Person.First(e => e.ID == p.ID);
            var temp2 = x.Site.First(e => e.Id == s.Id);
            temp.Details.Site = s;

            x.Person.AddOrUpdate(temp);
            return SaveChanges();

        }

        public bool ChangeRole(int eID, int rID)
        {
            var employee = GetEmployee(eID);
            var role = x.Role.First(e => e.ID == rID);
            employee.Role = role;
            employee.DateModified = DateTime.Now;
            employee.Active = true;
            x.Person.AddOrUpdate(employee);
            return SaveChanges();
        }

        public bool AddProjectTask(AcceptedProject ap, ProjectTask pt)
        {
            var temp = x.AcceptedProject.First(e => e.Id == ap.Id);
            pt.AcceptedProject = temp;
            x.Task1.AddOrUpdate(pt);
           
            return SaveChanges();
        }

   
        public bool RemoveEmployee(Person p)
        {
            var temp = x.Person.First(e => e.ID == p.ID);
            x.Person.Remove(temp);
            return SaveChanges();
        }
        
        public bool AddDayToSchedule(int Id, DateTime d)
        {
            
            var employee = GetEmployee(Id);
            var day = new Schedule() { DateWorking = d.Date, Person = employee,Active = true,DateModified = DateTime.Now};
            x.Schedule.Add(day);
            return SaveChanges();
            

        }

        public bool SaveChanges()
        {
            x.SaveChanges();
            try
            {
                
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
            
                
            
          
             
        }

        public bool CreateOrUpdateProject(AcceptedProject ap)
        {
            x.AcceptedProject.AddOrUpdate(ap);
            return SaveChanges();
        }

        public Person getManagerFor(int id)
        {
            try
            {
                var z = x.Manager.FirstOrDefault(e => e.EmployeeId == id).Person1;
                return z;
            }
            catch (NullReferenceException ex)
            {
                
                return new Person() {FirstName = "NOT ASSIGNED"};
            }
            
          
        }

        public Employee GenerateEmployee(int id)
        {
            return new EmployeeFactory().CreateEmployee(id);
        }

        public IEnumerable<Role> GetRoles()
        {
            return x.Role;
        }

        public IEnumerable<Status> GetStatuses()
        {
            return x.Status;
        }
        public Details GetEmployeeDetails(int ID)
        {
            return x.Person.First(e => e.ID == ID).Details;
        }

        public Role GetEmployeeRole(int id)
        {
            return x.Person.First(e => e.ID == id).Role;
        }

        public bool AddPersonTask(PersonTask pt)
        {
            x.Task.Add(pt);
            return SaveChanges();
        }

        public bool RemoveDayFromSchedule(int id)
        {

          
            try
            {
                var day = x.Schedule.First(e => e.Id == id);
                x.Schedule.Remove(day);

                return SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
            

        }

        public IEnumerable<PersonTask> GetPersonTasks()
        {
            return x.Task;
        }

        public bool ChangeStatus(int taskID, int statusID)
        {
            var task = x.Task.First(e => e.Id == taskID);
            task.StatusID = statusID;
            task.DateModified = DateTime.Now;
            x.Task.AddOrUpdate(task);
            return SaveChanges();
        }

        public bool RemoveTask(int id)
        {
            x.Task.Remove(x.Task.First(e => e.Id == id));
            return SaveChanges();
        }
    }
}
