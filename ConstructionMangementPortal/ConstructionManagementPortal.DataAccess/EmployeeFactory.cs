﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionManagementPortal.DataAccess
{
   public class EmployeeFactory
    {
        EFManagement efm = new EFManagement();
        public Employee CreateEmployee(int id)
        {
            var tempEmployee = efm.GetEmployee(id);
            return new Employee()
            {
                FirstName = tempEmployee.FirstName,
                LastName = tempEmployee.LastName,
                ID = id,
                AssignedTasks = efm.GetEmployeeTasks(id),
                DaysScheduled = efm.GetSchedule(id), 
                SupervisorEmployee = efm.getManagerFor(id),
                Details = efm.GetEmployeeDetails(id),
                Role = efm.GetEmployeeRole(id)
            };
        }
    }
}
