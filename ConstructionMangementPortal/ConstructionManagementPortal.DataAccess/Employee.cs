﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionManagementPortal.DataAccess
{
    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ID { get; set; }
        public Details Details { get; set; }
        public IEnumerable<PersonTask> AssignedTasks { get; set; }
        public IEnumerable<Schedule> DaysScheduled { get; set; }
        public Person SupervisorEmployee { get; set; }
        public Role Role { get; set; }
    }
}
