﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionManagementPortal.DataAccess
{
    public class UserNotUniqueException: Exception
    {
        public UserNotUniqueException()
        {
            
        }

        public UserNotUniqueException(string message)
            :base(message)
        {   
            
        }

        public UserNotUniqueException(string message, Exception innerException)
            :base(message, innerException)
        {
            
        }
    }
}
