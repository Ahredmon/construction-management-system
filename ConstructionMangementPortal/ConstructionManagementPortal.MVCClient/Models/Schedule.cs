﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConstructionManagementPortal.MVCClient.Models
{
    public class Schedule
    {
        public int Id { get; set; }
        public int EmployeeID { get; set; }
        [DataType(DataType.Date)]
        [Required]
        public System.DateTime DateWorking { get; set; }
       
    }
}