﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstructionManagementPortal.MVCClient.Models
{
    public class Workday
    {
        public int Id { get; set; }
        public System.TimeSpan TimeIn { get; set; }
        public System.TimeSpan? TimeOut { get; set; }
        public DateTime? DateModified { get; set; }
        public int EmployeeId { get; set; }
    }
}