﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstructionManagementPortal.MVCClient.Models
{
    public class NoUserAccessException:Exception
    {
        public NoUserAccessException()
        {
            
        }

        public NoUserAccessException(string message)
            :base(message)
        {
            
        }

        public NoUserAccessException(string message, Exception innerException)
            : base(message, innerException)
        {
            
        }
    }
}