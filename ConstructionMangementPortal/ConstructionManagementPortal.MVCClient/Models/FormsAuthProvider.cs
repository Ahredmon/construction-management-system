﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ConstructionManagementPortal.MVCClient.Models
{
    public class FormsAuthProvider : IAuthProvider
    {
        public int Authenticate(string username, string password)
        {
            var Employees = ClientHelper.Getemployess();

            try
            {
                var AuthenticatedEmployee =
                 Employees.First(e => e.Details.username == username && e.Details.password == password);
      
                var temprole = AuthenticatedEmployee.Role.ID;
                if (temprole == 4)
                {
                    
                    FormsAuthentication.SetAuthCookie(username, false);
                }
                return temprole;
            }
            catch (Exception)
            {
               
                return 0;
            }
           
        }
    }
}