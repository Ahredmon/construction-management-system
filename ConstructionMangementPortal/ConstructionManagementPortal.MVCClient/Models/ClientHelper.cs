﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using ConstructionManagementPortal.Domain;

namespace ConstructionManagementPortal.MVCClient.Models
{
   
    public static class ClientHelper
    {
    
        public  static List<Role> getRoles()
        {
            DomainHelper dh = new DomainHelper();
            var temp = new List<Role>();
            foreach (var item in dh.GetRoles())
            {
                temp.Add(new Role()
                {
                   ID = item.ID,
                   Name = item.Name
                });
            }
            return temp;
        }

        public static  List<Employee> Getemployess()
        {
            DomainHelper dh = new DomainHelper();
            var temp = new List<Employee>();
            foreach (var item in dh.GetEmployees())
            {

                temp.Add(ClientMapper.FromDomain(item));
                
            } 
            return temp;
        }

        public static List<ProjectTask> GetProjectTasks()
        {
            DomainHelper dh = new DomainHelper();
            var temp = new List<ProjectTask>();
            foreach (var item in dh.GetProjectTasks())
            {
                temp.Add(ClientMapper.FromDomain(item));
            }
            return temp;
        }

        public static List<AcceptedProject> GetAcceptedProjects()
        {
            DomainHelper dh = new DomainHelper();
            var temp = new List<AcceptedProject>();
            foreach (var item in dh.GetAcceptedProject())
            {
                temp.Add(ClientMapper.FromDomain(item));
            }
            return temp;
        }

        public static bool AddEmployee(Employee e)
        {
            DomainHelper dh = new DomainHelper();
            return dh.AddEmployee(ClientMapper.ToDomain(e));
        }

        public static bool AddPersonTask(PersonTask pt)
        {
            
            DomainHelper dh = new DomainHelper();
         ;
            return dh.AddPersonTask(ClientMapper.ToDomain(pt));
        }

        public static bool AddSite(Site s)
        {
            DomainHelper dh = new DomainHelper();
            return dh.AddSite(ClientMapper.ToDomain(s));
        }
        public static  bool AddDayToSchedule(Schedule s)
        {
            DomainHelper dh = new DomainHelper();
            return dh.AddDayToSchedule(ClientMapper.ToDomain(s));
        }

        public static bool RemoveDayFromSchedule(Schedule s)
        {
            DomainHelper dh = new DomainHelper();
            return dh.RemoveDayFromSchedule(ClientMapper.ToDomain(s));
        }

        public static bool Clock(Employee s)
        {
            var temp = ClientMapper.ToDomain(s);
            if (!temp.ClockIn())
            {
                return temp.ClockOut();
            }
            else return true;
        }

        public static Site GetSiteInformation(int id)
        {
            DomainHelper dh = new DomainHelper();
            return ClientMapper.FromDomain(dh.GetSiteInformation(id));
        }

        public static List<Workday> GetWorkdays()
        {
            DomainHelper dh = new DomainHelper();
            var temp = new List<Workday>();
            foreach (var item in dh.GetWorkDays())
            {
                temp.Add(ClientMapper.FromDomain(item));
            }
            return temp;
        }

        public static List<Status> GetStatuses()
        {
            DomainHelper dh = new DomainHelper();
            var temp = new List<Status>();
            foreach (var item in dh.GetStatuses())
            {
                temp.Add(ClientMapper.FromDomain(item));
            }

            return temp;
        }

        public static List<PersonTask> GetPersonTassk()
        {
            DomainHelper dh = new DomainHelper();
            var temp = new List<PersonTask>();
            foreach (var item in dh.GetTasks())
            {
                temp.Add(ClientMapper.FromDomain(item));
            }
            return temp;
        }

        public static bool ChangeStatus(int taskID, int statusID)
        {
            DomainHelper dh = new DomainHelper();
            return dh.ChangeStatus(taskID, statusID);
        }

        public static bool RemoveTask(int id)
        {
            return new DomainHelper().RemoveTask(id);
        }

    }
}