﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConstructionManagementPortal.MVCClient.Models
{
    public class Employee
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public int ID { get; set; }
        [Required]
        public Details Details { get; set; }
        public List<PersonTask> AssignedTasks { get; set; }
        public List<Schedule> DaysScheduled { get; set; }
        public Person SupervisorEmployee { get; set; }
        public int RoleID { get; set; }
        public Role Role { get; set; }

     
      }
}