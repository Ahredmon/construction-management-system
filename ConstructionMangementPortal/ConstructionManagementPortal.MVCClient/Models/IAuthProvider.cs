﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstructionManagementPortal.MVCClient.Models
{
    public interface IAuthProvider
    {
       int Authenticate(string username, string password);
    }
}