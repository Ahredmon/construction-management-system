﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstructionManagementPortal.MVCClient.Models
{
    public class ProjectTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int HoursBudgeted { get; set; }
        public double HoursWorked { get; set; }
        public int StatusID { get; set; }
        public int ProjectID { get; set; }
    }
}