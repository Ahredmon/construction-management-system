﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstructionManagementPortal.MVCClient.Models
{
    public class DateCount
    {
        public string Date { get; set; }
        public int Count { get; set; }
    }
}