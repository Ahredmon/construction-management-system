﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstructionManagementPortal.MVCClient.Models
{
    public class AcceptedProject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryID { get; set; }
        public int ManagerID { get; set; }
        public double HoursBudgeted { get; set; }
        public double HoursWorked { get; set; }
        public int SiteID { get; set; }
    }
}