﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstructionManagementPortal.Domain;

namespace ConstructionManagementPortal.MVCClient.Models
{
    internal static class ClientMapper
    {

        public static Site FromDomain(SiteDTO s)
        {
            return new Site
            {
                City = s.City,
                ContactName = s.ContactName,
                Id = s.Id,
                Name = s.Name,
                Phone = s.Phone,
                State = s.State,
                Street = s.State,
                Zip = s.Zip
            };
        }

        public static SiteDTO ToDomain(Site s)
        {
            return new SiteDTO()
            {
                City = s.City,
                ContactName = s.ContactName,
                Id = s.Id,
                Name = s.Name,
                Phone = s.Phone,
                State = s.State,
                Street = s.State,
                Zip = s.Zip
            };
        }


        public static Employee FromDomain(EmployeeDAO e)
        {
            List<PersonTask> temp = new List<PersonTask>();
            foreach (var item in e.AssignedTasks)
            {
                temp.Add(ClientMapper.FromDomain(item));
            }
            List<Schedule> tempSchedules = new List<Schedule>();
            foreach (var item in e.DaysScheduled)
            {
                tempSchedules.Add(ClientMapper.FromDomain(item));
            }
            return new Employee()
            {
                AssignedTasks = temp,
                DaysScheduled = tempSchedules,
                FirstName = e.FirstName,
                LastName = e.LastName,
                ID = e.ID,
                SupervisorEmployee = ClientMapper.FromDomain(e.SupervisorEmployee),
                Details = ClientMapper.FromDomain(e.DetailsDao),
                Role = ClientMapper.FromDomain(e.RoleDto)

            };
        }

        public static EmployeeDAO ToDomain(Employee e)
        {

            return new EmployeeDAO()
            {

                FirstName = e.FirstName,
                LastName = e.LastName,
                ID = e.ID,
                RoleDto = ClientMapper.ToDomain(e.Role),
                DetailsDao = ClientMapper.ToDomain(e.Details),


            };
        }

        public static Person FromDomain(PersonDAO p)
        {
            return new Person()
            {
                FirstName = p.FirstName,
                LastName = p.LastName,
                ID = p.ID
            };
        }

        public static PersonDAO ToDomain(Person p)
        {
            return new PersonDAO()
            {
                FirstName = p.FirstName,
                LastName = p.LastName,
                ID = p.ID
            };
        }


        public static PersonTask FromDomain(PersonTaskDAO ptd)
        {
            return new PersonTask()
            {
                EmployeeId = ptd.EmployeeId,
                StatusID = ptd.StatusID,
                HoursWorked = ptd.HoursWorked,
                HoursBudgeted = ptd.HoursBudgeted,
                Id = ptd.Id,
                ProjectId = ptd.Id,
                Name = ptd.Name
            };
        }

        public static PersonTaskDAO ToDomain(PersonTask ptd)
        {
            return new PersonTaskDAO()
            {
                EmployeeId = ptd.EmployeeId,
                StatusID = ptd.StatusID,
                HoursWorked = ptd.HoursWorked,
                HoursBudgeted = ptd.HoursBudgeted,
                Id = ptd.Id,
                ProjectId = ptd.ProjectId,
                Name = ptd.Name
            };
        }


        public static Schedule FromDomain(ScheduleDAO sd)
        {
            return new Schedule()
            {
                DateWorking = sd.DateWorking,
                EmployeeID = sd.EmployeeID,
                Id = sd.Id
            };
        }

        public static ScheduleDAO ToDomain(Schedule sd)
        {
            return new ScheduleDAO()
            {
                DateWorking = sd.DateWorking,
                EmployeeID = sd.EmployeeID,
                Id = sd.Id
            };
        }


        public static DetailsDAO ToDomain(Details dd)
        {
            return new DetailsDAO()
            {
                DateHired = dd.DateHired,
                ID = dd.ID,
                password = dd.password,
                phone = dd.phone,
                SiteID = dd.SiteID,
                SSN = dd.SSN,
                username = dd.username
            };
        }

        public static Details FromDomain(DetailsDAO dd)
        {
            return new Details()
            {
                DateHired = dd.DateHired,
                ID = dd.ID,
                password = dd.password,
                phone = dd.phone,
                SiteID = dd.SiteID,
                SSN = dd.SSN,
                username = dd.username
            };
        }

        public static Role FromDomain(RoleDTO rt)
        {
            return new Role()
            {
                Name = rt.Name,
                ID = rt.ID

            };
        }

        public static RoleDTO ToDomain(Role rt)
        {
            return new RoleDTO()
            {
                Name = rt.Name,
                ID = rt.ID

            };
        }

        public static ProjectTaskDTO ToDomain(ProjectTask pt)
        {
            return new ProjectTaskDTO()
            {
                Description = pt.Description,
                HoursWorked = pt.HoursWorked,
                HoursBudgeted = pt.HoursBudgeted,
                Id = pt.Id,
                StatusID = pt.StatusID,
                Name = pt.Name,
                ProjectID = pt.ProjectID
            };
        }

        public static ProjectTask FromDomain(ProjectTaskDTO pt)
        {
            return new ProjectTask()
            {
                Description = pt.Description,
                HoursWorked = pt.HoursWorked,
                HoursBudgeted = pt.HoursBudgeted,
                Id = pt.Id,
                StatusID = pt.StatusID,
                Name = pt.Name,
                ProjectID = pt.ProjectID,
               
            };
        }

        public static AcceptedProjectDTO ToDomain(AcceptedProject ap)
        {
            return new AcceptedProjectDTO()
            {
                CategoryID = ap.CategoryID,
                HoursWorked = ap.HoursWorked,
                HoursBudgeted = ap.HoursBudgeted,
                Id = ap.Id,
                ManagerID = ap.ManagerID,
                Name = ap.Name,
                SiteID = ap.SiteID

            };
        }

        public static AcceptedProject FromDomain(AcceptedProjectDTO ap)
        {
            return new AcceptedProject()
            {
                CategoryID = ap.CategoryID,
                HoursWorked = ap.HoursWorked,
                HoursBudgeted = ap.HoursBudgeted,
                Id = ap.Id,
                ManagerID = ap.ManagerID,
                Name = ap.Name,
                SiteID = ap.SiteID

            };
        }

        public static Workday FromDomain(WorkDayDTO w)
        {
            return new Workday()
            {
                EmployeeId = w.EmployeeId,
                Id = w.Id,
                TimeOut = w.TimeOut,
                TimeIn = w.TimeIn,
                DateModified = w.DateModified
            };


        }

        public static Status FromDomain(StatusDTO s)
        {
            return new Status()
            {
                Id = s.Id,
                Name = s.Name
            };
        }
    }
}


    