﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConstructionManagementPortal.MVCClient.Models;
using Ninject;
using Employee = ConstructionManagementPortal.MVCClient.Models.Employee;

namespace ConstructionManagementPortal.MVCClient.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
       
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }

        // GET: Default/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Default/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Default/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Default/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Default/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Default/Delete/5
        public ActionResult Delete(int id)
        {
            ClientHelper.RemoveTask(id);
            return RedirectToAction("Index");
        }

        // POST: Default/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Roles()
        {
            var temp = ClientHelper.getRoles();

            return View(temp);
        }

        public ActionResult Employees()
        {
            return View(ClientHelper.Getemployess());
        }

       
        public ActionResult EmployeeDetails(int id)
        {
            return View(ClientHelper.Getemployess().First(e => e.ID == id));
        }

        public ActionResult CreateEmployee()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult CreateEmployee(Employee e)
        {
            if (!ModelState.IsValid) return View();
            ClientHelper.AddEmployee(e);
            
           return RedirectToAction("Employees");
        

        }

        public ActionResult ChangeRole(int id)
        {
            
            return View(ClientHelper.Getemployess().First(e => e.ID == id));
        }

        [HttpPost]
        public ActionResult ChangeRole(Employee e)
        {


            e.Details = ClientHelper.Getemployess().First(x => x.ID == e.ID).Details; 
            ClientHelper.AddEmployee(e);
            return RedirectToAction("Employees");
        }

        public ActionResult ViewSchedule(int id)
        {
            var temp = new List<Schedule>();
            try
            {
                foreach (var item in ClientHelper.Getemployess().First(e => e.ID == id).DaysScheduled)
                {
                    temp.Add(item);
                }

            }
            catch 
            {
                
            }
           
            return View(temp);
        }

        public ActionResult AddSchedule()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddSchedule(Schedule schedule)
        {
            
            if (ModelState.IsValid)
            {
                if (ClientHelper.AddDayToSchedule(schedule))
                {
                    ModelState.AddModelError("", "Day was successfully added");
                }
                else
                {
                    ModelState.AddModelError("", string.Format("{0} cannot work twice on {1}",schedule.EmployeeID,schedule.DateWorking));
                }

            }
           
            return View();
        }

        public ActionResult AcceptedProjects()
        {
            return View(ClientHelper.GetAcceptedProjects());
        }

        public ActionResult RemoveDay(Schedule schedule, string returnURL)
        {
            if (ClientHelper.RemoveDayFromSchedule(schedule))
            {
                return Redirect(returnURL ?? Url.Action("Employees"));
            }
            return RedirectToAction("Index", "Admin");
        }

        public ActionResult NewPersonTask()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewPersonTask(PersonTask p)
        {
            ClientHelper.AddPersonTask(p);
            return RedirectToAction("EmployeeDetails", new {id = p.EmployeeId});
        }

        public ActionResult EmployeeTask(int id)
        {
            return View(ClientHelper.GetPersonTassk().Where(e=> e.EmployeeId == id));
        }


        public static  List<SelectListItem> GetDropDown()
        {
            
            List<SelectListItem> ls = new List<SelectListItem>();
            foreach (var item in ClientHelper.getRoles())
            {
                ls.Add( new SelectListItem() {Text = item.Name,Value = item.ID.ToString()});
            }
            return ls;
        }

        public static List<SelectListItem> GetEmployeeDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();
            foreach (var item in ClientHelper.Getemployess())
            {
                ls.Add(new SelectListItem() {Text = item.FirstName + " " + item.LastName,Value = item.ID.ToString()});
            }
            return ls;
        }

        public static Site GetSiteInformation(int id)
        {
            return ClientHelper.GetSiteInformation(id);
        }

        public static Employee GetEmployeeInformation(int id)
        {
            return ClientHelper.Getemployess().First(e => e.ID == id);
        }

        public static List<SelectListItem> GetProjectsDropDown()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var item in ClientHelper.GetProjectTasks())
            {
                list.Add(new SelectListItem() {Text = item.Name, Value = item.Id.ToString()});
            }
            return list;
        }

        public static List<SelectListItem> GetStatusDropDowns()
        {
            var temp = new List<SelectListItem>();
            foreach (var item in ClientHelper.GetStatuses())
            {
                temp.Add(new SelectListItem() { Text = item.Name, Value = item.Id.ToString() });
            }

            return temp;
        }

     
        

    }
}
