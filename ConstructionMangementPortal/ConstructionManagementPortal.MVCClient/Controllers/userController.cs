﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConstructionManagementPortal.MVCClient.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ConstructionManagementPortal.MVCClient.Controllers
{
    
    public class userController:Controller
    {
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult EnterUsername()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EnterUsername(Employee e)
        {
            var temp =
                ClientHelper.Getemployess().First(x => x.Details.username.ToLower() == e.Details.username.ToLower());

            if (
                ClientHelper.Clock(temp))
            {
                TempData["Greeting"] = string.Format("Thank you {0} {1}, you have been clocked in/out ", temp.FirstName,
                    temp.LastName);
                TempData["Person"] = temp;
                return RedirectToAction("AssignedTasks",new {id = temp.ID});
                
            }
            TempData["Error"] = "You have already clocked out for the day";
            return RedirectToAction("EnterUsername");
        }

        public ActionResult AssignedTasks(int id )
        {
            var l = ClientHelper.GetPersonTassk().Where(e=> e.EmployeeId == id);
            if (l.Any())
            {
                return View(l);
            }
            return RedirectToAction("Index");
        }
    
       
        public ActionResult ListPersonTasks()
        {

            var temp = new List<PersonTask>();
            foreach (var item in ClientHelper.GetPersonTassk())
            {
                temp.Add(item);
            }

            return View(temp);
        }

        public ActionResult JsonString()
        {
           var temp = ClientHelper.GetWorkdays().GroupBy(e=> e.DateModified.Value.Date);
           var dateList = new List<DateCount>();

            foreach (var item in temp)
            {
               dateList.Add(new DateCount()
               {
                   Count = item.Count(),
                   Date = item.First().DateModified.Value.Date.ToString("d")
       
               });
            }

          
            
            var Json = JsonConvert.SerializeObject(dateList.Skip(dateList.Count - 7));
            return Content(Json, "application/json");
        }

        public ActionResult ActivateTask(int id)
        {
            ClientHelper.ChangeStatus(id, 6);
            return RedirectToAction("AssignedTasks",
               new {id = ClientHelper.GetPersonTassk().First(e => e.Id == id).EmployeeId });
        }

        public ActionResult CompleteTask(int id)
        {
            ClientHelper.ChangeStatus(id, 7);
            return RedirectToAction("AssignedTasks",
                new {id=ClientHelper.GetPersonTassk().First(e => e.Id == id).EmployeeId});
        }
    }
}