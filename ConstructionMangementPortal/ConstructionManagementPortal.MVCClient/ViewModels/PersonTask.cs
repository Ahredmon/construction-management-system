﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstructionManagementPortal.MVCClient.Models;

namespace ConstructionManagementPortal.MVCClient.ViewModels
{
    public class PersonTaskVm
    {
        public int Id { get; set; }
        public ProjectTask ProjectTask { get; set; }
        public Employee Employee { get; set; }
        public int HoursBudgeted { get; set; }
        public double HoursWorked { get; set; }
        public Status Status { get; set; }
        public string Name { get; set; }

    }
}