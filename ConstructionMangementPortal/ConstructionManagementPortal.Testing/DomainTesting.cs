﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstructionManagementPortal.Domain;
using Xunit;
using Xunit.Abstractions;
namespace ConstructionManagementPortal.Testing
{
    public class DomainTesting
    {
        DomainHelper dh = new DomainHelper();
        private readonly ITestOutputHelper output;
        public DomainTesting(ITestOutputHelper _output)
        {
            output = _output;
        }

        [Fact]
        public void GetEmployees()
        {
            var temp = dh.GetEmployees();
            foreach (var item in temp)
            {
                output.WriteLine(item.FirstName);
            }
        }

        [Fact]
        public void GetRoles()
        {
            var temp = dh.GetRoles();
            foreach (var item in temp)
            {
                output.WriteLine(item.Name);
            }
        }

    }
}
