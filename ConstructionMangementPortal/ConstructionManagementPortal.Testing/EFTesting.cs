﻿//using System;
//using System.CodeDom.Compiler;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using ConstructionManagementPortal.DataAccess;
//using Moq;
//using Xunit;
//using Xunit.Abstractions;
//using Xunit.Sdk;

//namespace ConstructionManagementPortal.Testing
//{
//    public class EFTesting
//    {
//        private readonly ITestOutputHelper output = new TestOutputHelper();
//        EFManagement db = new EFManagement();

//        public EFTesting(ITestOutputHelper _output)
//        {
//            output = _output;
//        }

//        [Fact]
//        public void Employee_Access_Test()
//        {
//            var x = db.GetEmployees();
//            Assert.True(x.Count() >= 0);
//            var temp = x.First(e => e.ID == 1).DateModified.ToString();
//            var z = DateTime.Parse(temp);
//            if (z.Date == DateTime.Now.Date)
//            {
//                output.WriteLine("Same Day");
//            }

//        }

//        [Fact]
//        public void Add_Employee_Test()
//        {

//            var p = new Person()
//            {
//                FirstName = "George",
//                LastName = "Bob",
//                RoleID = 2
//            };
//            var d = new Details()
//            {
//                SSN = "123321234",
//                password = "testMe",
//                username = "Test2",
//                DateHired = DateTime.Now,
//                SiteID = 1,
//                phone = "123456789012"


//            };
//            Assert.True(db.AddEmployee(p, d));
//        }

//        [Fact]
//        public void Clock_in_test()
//        {
//            db.ClockIn(6);
//        }

//        [Fact]
//        public void Clock_Out_Test()
//        {
//            db.ClockOut(6);
//        }

//        [Fact]
//        public void Add_Day_Schedule()
//        {
//            db.AddDayToSchedule(1, DateTime.Now.Date);
//        }

//        [Fact]
//        public void Get_Schedule()
//        {
//            var days = db.GetSchedule(1);
//            foreach (var item in days)
//            {
//                output.WriteLine(item.DateWorking.ToString());
//            }
//        }

//        [Fact]
//        public void Change_Role()
//        {
//            db.ChangeRole(3, 2);
//        }

//        [Fact]
//        public void AddOrUpdateProject()
//        {
//            var project = new AcceptedProject()
//            {
//                Active = true,
//                DateModified = DateTime.Now,
//                CategoryID = 1,
//                HoursBudgeted = 1200,
//                HoursWorked = 0,
//                ManagerID = 1,
//                Name = "Revature Main Campus",
//                SiteID = 1,
//                StatusID = 4

//            };
//            db.CreateOrUpdateProject(project);

//        }

//        [Fact]
//        public void AddProjectTask()
//        {
//            var ap = new AcceptedProject() {Id = 7};
//            var task = new ProjectTask()
//            {
//                HoursBudgeted = 60,
//                HoursWorked = 0,
//                Description = "Survey Land",
//                Active = true,
//                DateModified = DateTime.Now,
//                StatusID = 4,
//                Name = "Task 1",

//            };
//            db.AddProjectTask(ap, task);
//        }

//        [Fact]
//        public void AddPersonTask()
//        {
//            var person = new Person() {ID = 1};
//            var task = new PersonTask()
//            {
//                DateModified = DateTime.Now,
//                Active = true,
//                ProjectId = 3,
//                HoursBudgeted = 20,
//                HoursWorked = 0,
//                StatusID = 4

//            };
//            db.AddPersonTask(person, task);
//        }

//        [Fact]
//        public void GenerateEmployee()
//        {
//            var x = new EmployeeFactory();
//            var employee = x.CreateEmployee(1);
//            output.WriteLine(employee.FirstName);

//        }

//        [Fact]
//        public void GetTasks()
//        {
//            foreach (var item in db.GetEmployeeTasks(1))
//            {
//                output.WriteLine(item.ProjectTask.Name);
//            }

//        }

//        [Fact]
//        public void orderbytest()
//        {
//            var tempx = db.GetWorkDays().GroupBy(e => e.DateModified.Value.Date); foreach (var item in tempx)
//            {
//                output.WriteLine(item.Count().ToString());
//            }
           
//        }
//    }
//}
