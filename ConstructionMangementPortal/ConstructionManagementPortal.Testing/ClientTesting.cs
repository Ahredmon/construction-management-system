﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstructionManagementPortal.MVCClient.Controllers;
using ConstructionManagementPortal.MVCClient.Models;
using Moq;
using Xunit;
using Xunit.Abstractions;
using System.Web.Mvc;

namespace ConstructionManagementPortal.Testing
{
    public class ClientTesting
    {
        private readonly ITestOutputHelper Output;

        public ClientTesting(ITestOutputHelper output)
        {
            Output = output;
        }

        [Fact]
        public void BadAuth()
        {

            Mock<IAuthProvider> mock = new Mock<IAuthProvider>();
            mock.Setup(m => m.Authenticate("Ahredmon", "Redmon")).Returns(4);
            LoginVM model = new LoginVM()
            {
                username="Ahredmon",
                password = "Redmon"
            };

            AccountController target = new AccountController(mock.Object);
            ActionResult result = target.Login(model, "/MyUrl");
        }
    }
}
