
create schema Employee;
go
create schema Inventory;
go
create schema Project;
go
create schema Company;
go

-- create tables;
go

--- Employee Schema -----

create table Employee.Person
(
  ID int identity(1,1) not null,
  RoleID int not null,
  FirstName nvarchar(25) not null,
  LastName nvarchar(25) not null,
  DateModified datetime2(0)  null default sysutcdatetime(),
  Active bit null default 1
)
create table Employee.[Role]
(
  ID int identity(1,1) not null,
  Name nvarchar(25) not null,
  DateModified datetime2(0) null default sysutcdatetime(),
  Active bit null default 1
)

go
create table Employee.WorkDay(
Id int identity(1,1) not null,
TimeIn time not null,
[TimeOut] time not null,
DateModified datetime2(0) null default sysutcdatetime(),
Active bit null default 1
)

create table Employee.Manager(
ID int identity(1,1) not null,
ManagerId int not null,
EmployeeId int not null,
DateModified datetime2(0) null default sysutcdatetime(),
Active bit null default 1
)
create table Employee.Details(
ID int identity(1,1) not null,
SiteID int not null,
SSN nvarchar(11) not null,
phone nvarchar(12) not null,
username nvarchar(25) not null,
DateHired datetime2(0) not null,
[password] nvarchar(25) not null,
DateModified datetime2(0) null default sysutcdatetime(),
Active bit null default 1
)
create table Employee.Assignment(
ID int identity(1,1) not null,
EmployeeID int not null,
ProjectID int not null,
StartDate date not null,
EndTime date not null,
DateModified datetime2(0) null default sysutcdatetime(),
Active bit null default 1
)
create table Employee.Task(
Id int identity(1,1) not null,
ProjectId int not null,
EmployeeId int not null,
HoursBudgeted int not null,
HoursWorked int not null,
DateModified datetime2(0) null default sysutcdatetime(),
Active bit null default 1
)
create table Employee.Schedule(
Id int identity(1,1) not null,
EmployeeID int not null,
DateModified datetime2(0) null default sysutcdatetime(),
Active bit null default 1
)


----- Iventory Schema -----

create table Inventory.Product(

  ID int identity(1,1) not null,
  Name nvarchar(25) not null,
  [Description] nvarchar(255) not null,
  Stock int null default 0,
  DateModified datetime2(0)  null default sysutcdatetime(),
  Active bit null default 1
  )
create table Inventory.Description(
  ID int identity(1,1) not null,
  [Description] nvarchar(255) not null,
  [Weight] int not null,
  Price money not null,


  )
create table Inventory.Category(
 ID int identity(1,1) not null,
 Name nvarchar(25) not null,
 DateModified datetime2(0)  null default sysutcdatetime(),
 Active bit null default 1
 )

 go

 -- project schema ---
 create table Project.AcceptedProject(
 Id int identity(1,1) not null,
 Name nvarchar(25) not null,
 CategoryID int not null,
 ManagerID int not null,
 HoursBudgeted float not null,
 HoursWorked float not null,
 DateModified datetime2(0)  null default sysutcdatetime(),
 Active bit null default 1
 )
create table Project.Task
(
Id int identity(1,1) not null,
Name nvarchar(25) not null,
[Description] nvarchar(255) not null,
HoursBudgeted int not null,
HoursWorked int not null,
StatusID int not null,
 DateModified datetime2(0)  null default sysutcdatetime(),
 Active bit null default 1
 )
 create table Project.Status
 (
 Id int identity(1,1) not null,
 Name nvarchar(10) not null,
 DateModified datetime2(0)  null default sysutcdatetime(),
 Active bit null default 1
 )
 go
 create table Company.Vendor
 (
 Id int identity(1,1) not null,
 Name nvarchar(50) not null,
 Street nvarchar(100) not null,
 City nvarchar(100) not null,
 [State] nvarchar(50) not null,
 Zip nvarchar(5) not null,
 Phone nvarchar(12) not null,
 ContactName nvarchar(100) not null,
 DateModified datetime2(0)  null default sysutcdatetime(),
 Active bit null default 1
 )
 create table Company.Category
 (
 Id int identity(1,1) not null,
 Name nvarchar(25) not null,
  DateModified datetime2(0)  null default sysutcdatetime(),
 Active bit null default 1
 )
 create table Company.[Order]
 (
  Id int identity(1,1) not null,
  ProductID int not null,
  Quantity int not null,
   DateModified datetime2(0)  null default sysutcdatetime(),
 Active bit null default 1
 )


 Create table [Site]
 (
 Id int identity(1,1) not null,
 Name nvarchar(50) not null,
 Street nvarchar(100) not null,
 City nvarchar(100) not null,
 [State] nvarchar(50) not null,
 Zip nvarchar(5) not null,
 Phone nvarchar(12) not null,
 ContactName nvarchar(100) not null,
 DateModified datetime2(0)  null default sysutcdatetime(),
 Active bit null default 1
 )

 alter table Employee.Person
 add  DetailsID int not null

 alter table Employee.WorkDay
 add EmployeeId int not null
 
 alter table Project.AcceptedProject
 add SiteID int not null

 create table Employee.Requests
 (
  Id int identity(1,1) primary key,
  EmployeeID int not null,
  ProductID int not null,
  Quantity int not null,
  DateRequested int not null,
  DateNeeded int not null,
  [StatusID] int not null,
  DateModified datetime2(0)  null default sysutcdatetime(),
  Active bit null default 1


 )
 create table Employee.RequestStatus
 (
 Id int identity(1,1) primary key, 
 Name nvarchar(10) not null,
 DateModified datetime2(0)  null default sysutcdatetime(),
 Active bit null default 1
 )
 alter table Employee.Task
 add StatusID int not null

 create table Project.Category
 (
 id int identity(1,1) primary key,
 name nvarchar(10) not null,
  DateModified datetime2(0)  null default sysutcdatetime(),
 Active bit null default 1
 )

 alter table employee.person
 add DetailsID int not null