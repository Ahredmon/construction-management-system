alter table Employee.Person
add constraint pk_Employee_Person primary key clustered (ID);

----

alter table Company.Category
add constraint PK_Company_Category primary key clustered (ID);

alter table Company.[Order]
add constraint pk_company_order primary key clustered (ID);
alter table Company.Vendor
add constraint pk_company_vendor primary key clustered (ID);
alter table employee.assignment
add constraint pk_employee_assignment primary key clustered (ID);
alter table employee.details
add constraint pk_employee_details primary key clustered (ID);
alter table employee.manager 
add constraint pk_employee_manager primary key clustered (ID);

alter table employee.[role]
add constraint pk_employee_role primary key clustered (ID);
alter table employee.schedule
add constraint pk_employee_schedule primary key clustered (ID);
alter table employee.task
add constraint pk_employee_task primary key clustered (ID);
alter table employee.workday 
add constraint pk_employee_workday primary key clustered (ID);
alter table inventory.category 
add constraint pk_inventory_category primary key clustered (ID);
alter table inventory.[description] 
add constraint pk_inventory_description primary key clustered (ID);
alter table inventory.product
add constraint pk_inventory_product primary key clustered (ID);
alter table project.acceptedproject 
add constraint pk_project_acceptedproject primary key clustered(ID);
alter table project.[status] 
add constraint pk_project_status primary key clustered(ID);
alter table project.task
add constraint pk_project_taskt primary key clustered (ID);
alter table employee.details
add constraint pk_employee_details primary key clustered(ID);
alter table [site]
add constraint pk_site primary key clustered (ID);

-- begin FK relationships

alter table Employee.Person
add constraint fk_employee_person foreign key(RoleID) references Employee.[Role](ID);

alter table Employee.Details
add constraint fk_employee_details foreign key(SiteId) references [site](id);

alter table Employee.WorkDay
add constraint fk_employee_workday foreign key(EmployeeID) references Employee.Person(ID);

alter table Employee.Manager
add constraint fk_employee_managerId foreign key(ManagerID) references Employee.Person(ID);

alter table Employee.Manager
add constraint fk_employee_employeeID foreign key(EmployeeID) references Employee.Person(ID);

alter table Employee.assignment
add constraint fk_assignment_employeeID foreign key(EmployeeID) references Employee.Person(ID);

alter table Employee.schedule 
add constraint fk_schedule_employeeID foreign key(EmployeeID) references employee.person(ID);

alter table Employee.Person
add constraint fk_person_DetailsID foreign key (DetailsID) references Employee.Details(id)

alter table inventory.product
add CategoryID int not null
alter table project.acceptedproject
add constraint fk_acceptedProject_categoryID foreign key(CategoryID) references project.category(ID);

alter table project.acceptedproject
add constraint fk_acceptedProject_managerID foreign key (ManagerId) references employee.person(ID);

alter table project.task
add constraint fk_task_ProjectID foreign key (projectID) references project.acceptedproject(id);


select * from project.task
select * from project.acceptedproject
select* from Employee.WorkDay
select * from Employee.Person
select * from Employee.Details