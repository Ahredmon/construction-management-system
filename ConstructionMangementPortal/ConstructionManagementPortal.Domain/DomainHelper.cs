﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstructionManagementPortal.DataAccess;

namespace ConstructionManagementPortal.Domain
{
    public class DomainHelper
    {
        EFManagement x = new EFManagement();

        public List<EmployeeDAO> GetEmployees()
        {
            var temp = new List<EmployeeDAO>();
            var curPeople = x.GetEmployees();
            foreach (var item   in curPeople)
            {
              temp.Add(DomainMapper.MapFromDataAccess(x.GenerateEmployee(item.ID)));  
            }
            return temp;
        }

        public List<RoleDTO> GetRoles()
        {
            var temp =  new List<RoleDTO>();
            foreach (var item in x.GetRoles())
            {
                temp.Add(DomainMapper.MapFromDataAccess(item)); 
            }
            return temp;
        }

        public List<ProjectTaskDTO> GetProjectTasks()
        {
            var temp = new List<ProjectTaskDTO>();
            foreach (var item in x.GetProjectTasks())
            {
                temp.Add(DomainMapper.MapFromDataAccess(item));
            }
            return temp;
        }

        public List<AcceptedProjectDTO> GetAcceptedProject()
        {
            var temp = new List<AcceptedProjectDTO>();
            foreach (var item in x.GetProjects())
            {
                temp.Add(DomainMapper.MayFromDataAccess(item));
            }
            return temp;
        }

        public bool AddEmployee(EmployeeDAO e )
        {
            var temp = new Person()
            {
                ID =  e.ID,
                FirstName =e.FirstName,
                LastName = e.LastName,
                Role = DomainMapper.MapToDataAccess(e.RoleDto),
                RoleID = e.RoleDto.ID
                
            };
            x.AddEmployee(temp, DomainMapper.MapToDataAccess(e.DetailsDao));
           
            return true;
            
        }

        public bool AddDayToSchedule(ScheduleDAO s)
        {
            
            foreach (var item in GetEmployees().First(z => z.ID == s.EmployeeID).DaysScheduled)
            {
                if (item.DateWorking == s.DateWorking)
                    return false;
            }
            return x.AddDayToSchedule(s.EmployeeID, s.DateWorking);
        }

        public bool AddSite(SiteDTO s)
        {
           return  x.AddSite(DomainMapper.MapToDataAccess(s));
        }

        public bool RemoveDayFromSchedule(ScheduleDAO s)
        {
           return x.RemoveDayFromSchedule(s.Id);
        }

        public SiteDTO GetSiteInformation(int id)
        {
            return DomainMapper.MapFromDataAcccess(x.GetSites().First(e => e.Id == id));
        }

        public bool AddPersonTask(PersonTaskDAO pt)
        {
            return x.AddPersonTask(DomainMapper.MapToDataAccess(pt));
        }

        public List<WorkDayDTO> GetWorkDays()
        {
            var temp = new List<WorkDayDTO>();
            foreach (var item in x.GetWorkDays())
            {
                temp.Add(DomainMapper.MapFromDataAccess(item));
            }
            return temp;
        }

        public List<StatusDTO> GetStatuses()
        {
            var temp = new List<StatusDTO>();
            foreach (var item in x.GetStatuses())
            {
                temp.Add(DomainMapper.MapFromDataAccess(item));

            }
            return temp;
        }

        public List<PersonTaskDAO> GetTasks()
        {
            var temp = new List<PersonTaskDAO>();
            foreach (var item in x.GetPersonTasks())
            {
                temp.Add(DomainMapper.MapFromDataAccess(item));
            }
            return temp;
        }

        public  bool ChangeStatus(int taskID, int statusID)
        {
            return x.ChangeStatus(taskID, statusID);
        }

        public bool RemoveTask(int id)
        {
            return x.RemoveTask(id);
        }
    }
}
