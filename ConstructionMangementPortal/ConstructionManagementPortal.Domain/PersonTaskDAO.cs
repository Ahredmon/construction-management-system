﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionManagementPortal.Domain
{
     public class PersonTaskDAO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int EmployeeId { get; set; }
        public int HoursBudgeted { get; set; }
        public double HoursWorked { get; set; }
        public int StatusID { get; set; }
         public string Name { get; set; }
    }
}
