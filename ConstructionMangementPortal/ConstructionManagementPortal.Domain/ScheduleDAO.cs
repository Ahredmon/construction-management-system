﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionManagementPortal.Domain
{
    public class ScheduleDAO
    {
        public int Id { get; set; }
        public int EmployeeID { get; set; }
        public System.DateTime DateWorking { get; set; }
        public virtual PersonDAO Person { get; set; }
    }
}
