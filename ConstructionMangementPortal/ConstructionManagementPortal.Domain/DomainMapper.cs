﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstructionManagementPortal.DataAccess;

namespace ConstructionManagementPortal.Domain
{
    public static class DomainMapper
    {

        public static EmployeeDAO MapFromDataAccess(Employee x)
        {
            var temp = new EmployeeDAO()
            {
                AssignedTasks = MapFromDataAccess(x.AssignedTasks),
                DaysScheduled = MapFromDataAccess(x.DaysScheduled),
                FirstName = x.FirstName,
                LastName = x.LastName,
                ID = x.ID,
                SupervisorEmployee = MapFromDataAccess(x.SupervisorEmployee),
                DetailsDao = MapFromDataAccess(x.Details),
                RoleDto = MapFromDataAccess(x.Role)
            };

            return temp;


        }

        public static PersonDAO MapFromDataAccess(Person p)
        {
            var person = new PersonDAO()
            {
                FirstName = p.FirstName,
                LastName = p.LastName,
                ID = p.ID
            };

            return person;

        }

        public static SiteDTO MapFromDataAcccess(Site s)
        {
            return new SiteDTO
            {
                City = s.City,
                ContactName = s.ContactName,
                Id = s.Id,
                Name = s.Name,
                Phone = s.Phone,
                State = s.State,
                Street = s.State,
                Zip = s.Zip
            };
        }

        public static Site MapToDataAccess(SiteDTO s)
        {
            return new Site
            {
                City = s.City,
                ContactName = s.ContactName,
                Id = s.Id,
                Name = s.Name,
                Phone = s.Phone,
                State = s.State,
                Street = s.State,
                Zip = s.Zip
            };
        }

        public static RoleDTO MapFromDataAccess(Role r)
        {
            return new RoleDTO()
            {
                ID = r.ID,
                Name = r.Name
            };
        }

        public static ProjectTaskDTO MapFromDataAccess(ProjectTask pt)
        {
            return new ProjectTaskDTO()
            {
                Description = pt.Description,
                HoursWorked = pt.HoursWorked,
                HoursBudgeted = pt.HoursBudgeted,
                Id = pt.Id,
                StatusID = pt.StatusID,
                Name = pt.Name,
                ProjectID = pt.ProjectID
            };
        }

        public static ProjectTask MapToDataAccess(ProjectTaskDTO pt)
        {
            return new ProjectTask()
            {
                Description = pt.Description,
                HoursWorked = pt.HoursWorked,
                HoursBudgeted = pt.HoursBudgeted,
                Id = pt.Id,
                StatusID = pt.StatusID,
                Name = pt.Name,
                ProjectID = pt.ProjectID
            };
        }

        public static Role MapToDataAccess(RoleDTO r)
        {
            return new Role()
            {
                ID = r.ID,
                Name = r.Name
            };
        }

        public static DetailsDAO MapFromDataAccess(Details d)
        {
            return new DetailsDAO()
            {
                DateHired = d.DateHired,
                ID = d.ID,
                SiteID = d.SiteID,
                password = d.password,
                phone = d.phone,
                SSN = d.SSN,
                username = d.username
            };
        }

        public static Details MapToDataAccess(DetailsDAO d)
        {
            return new Details()
            {
                DateHired = d.DateHired,
                ID = d.ID,
                SiteID = d.SiteID,
                password = d.password,
                phone = d.phone,
                SSN = d.SSN,
                username = d.username
            };
        }

        public static List<PersonTaskDAO> MapFromDataAccess(IEnumerable<PersonTask> pt)
        {
            List<PersonTaskDAO> ptd = new List<PersonTaskDAO>();
            foreach (var item in pt)
            {
                var tempDTO = new PersonTaskDAO()
                {
                    StatusID = item.StatusID,
                    EmployeeId = item.EmployeeId,
                    HoursWorked = item.HoursWorked,
                    HoursBudgeted = item.HoursBudgeted,
                    Id = item.Id,
                    ProjectId = item.ProjectId
                };
                ptd.Add(tempDTO);
            }
            return ptd;
        }

        public static List<ScheduleDAO> MapFromDataAccess(IEnumerable<Schedule> schedule)
        {
            var lsd = new List<ScheduleDAO>();
            foreach (var item in schedule)
            {
                var temp = new ScheduleDAO()
                {
                    DateWorking = item.DateWorking,
                    EmployeeID = item.EmployeeID,
                    Id = item.Id
                };
                lsd.Add(temp);
            }
            return lsd;

        }

        public static PersonTask MapToDataAccess(PersonTaskDAO ptd)
        {
            return new PersonTask()
            {
                Active = true,
                DateModified = DateTime.Now,
                EmployeeId = ptd.EmployeeId,
                HoursWorked = ptd.HoursWorked,
                HoursBudgeted = ptd.HoursBudgeted,
                ProjectId = ptd.ProjectId,
                StatusID = ptd.StatusID,
                Id = ptd.Id,
                Name = ptd.Name

            };
        }



        public static AcceptedProjectDTO MayFromDataAccess(AcceptedProject ap)
        {
            return new AcceptedProjectDTO()
            {
                CategoryID = ap.CategoryID,
                HoursWorked = ap.HoursWorked,
                HoursBudgeted = ap.HoursBudgeted,
                Id = ap.Id,
                ManagerID = ap.ManagerID,
                Name = ap.Name,
                SiteID = ap.SiteID

            };
        }

        public static AcceptedProject MayToDataAccess(AcceptedProjectDTO ap)
        {
            return new AcceptedProject()
            {
                CategoryID = ap.CategoryID,
                HoursWorked = ap.HoursWorked,
                HoursBudgeted = ap.HoursBudgeted,
                Id = ap.Id,
                ManagerID = ap.ManagerID,
                Name = ap.Name,
                SiteID = ap.SiteID

            };
        }

        public static PersonTaskDAO MapFromDataAccess(PersonTask p)
        {
            return new PersonTaskDAO()
            {
                EmployeeId = p.EmployeeId,
                Id = p.Id,
                HoursWorked = p.HoursWorked,
                HoursBudgeted = p.HoursBudgeted,
                ProjectId = p.ProjectId,
                StatusID = p.StatusID,
                Name = p.Name
            };

        }

        public static WorkDayDTO MapFromDataAccess(WorkDay wd)
        {
            return new WorkDayDTO()
            {
                DateModified = wd.DateModified,
                EmployeeId = wd.EmployeeId,
                Id = wd.Id,
                TimeOut = wd.TimeOut,
                TimeIn = wd.TimeIn
            };
        }

        public static StatusDTO MapFromDataAccess(Status s)
        {
            return new StatusDTO()
            {
                Id = s.Id,
                Name = s.Name
            };
        }
    }
}
