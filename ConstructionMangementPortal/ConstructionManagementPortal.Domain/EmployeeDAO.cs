﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstructionManagementPortal.DataAccess;

namespace ConstructionManagementPortal.Domain
{
     public class EmployeeDAO
    {  
        EFManagement efm = new EFManagement();
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ID { get; set; }
        public DetailsDAO DetailsDao { get; set; }
        public int RoleID { get; set; }
        public List<PersonTaskDAO> AssignedTasks { get; set; }
        public List<ScheduleDAO> DaysScheduled { get; set; }
        public PersonDAO SupervisorEmployee { get; set; }
         public RoleDTO RoleDto { get; set; }
   

         public bool ClockIn()
         {
             return efm.ClockIn(ID);
         }

         public bool ClockOut()
         {
             return efm.ClockOut(ID);
         }
         public bool ChangeRole(RoleDTO r)
         {
             try
             {
                efm.ChangeRole(ID, r.ID);
                 return true;
             }
             catch (Exception)
             {
                 
                 throw;
             }
            
         }
    }
}
