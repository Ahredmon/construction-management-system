﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionManagementPortal.Domain
{
   public class DetailsDAO
    {
        public int ID { get; set; }
        public int SiteID { get; set; }
        public string SSN { get; set; }
        public string phone { get; set; }
        public string username { get; set; }
        public System.DateTime DateHired { get; set; }
        public string password { get; set; }
    }
}
