﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionManagementPortal.Domain
{
    public class WorkDayDTO
    {
        public int Id { get; set; }
        public System.TimeSpan TimeIn { get; set; }
        public TimeSpan? TimeOut { get; set; }
        public DateTime? DateModified { get; set; }
        public int EmployeeId { get; set; }
    }
}
